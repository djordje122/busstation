package com.ftninformatika.jwd.autobuska.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.autobuska.model.Linija;
import com.ftninformatika.jwd.autobuska.model.Rezervacija;
import com.ftninformatika.jwd.autobuska.repository.LinijaRepository;
import com.ftninformatika.jwd.autobuska.repository.RezervacijaRepository;
import com.ftninformatika.jwd.autobuska.service.LinijaService;

@Service
public class JpaLinijaService implements LinijaService{

	@Autowired
	private LinijaRepository rep;
	
	@Autowired
	private RezervacijaRepository rezervacijaRepository;
	
	@Override
	public Linija findOneById(Long id) {
		// TODO Auto-generated method stub
		return rep.findOneById(id);
	}

	@Override
	public List<Linija> findAll(Integer pageNo) {
		// TODO Auto-generated method stub
		return rep.findAll();
	}

	@Override
	public Linija save(Linija t) {
		// TODO Auto-generated method stub
		return rep.save(t);
	}

	@Override
	public Linija update(Linija t) {
		// TODO Auto-generated method stub
		return rep.save(t);
	}

	@Override
	public Linija delete(Long id) {
		Optional<Linija> takmicenje=Optional.of(findOneById(id));
		if(takmicenje.isPresent()) {
			rep.deleteById(id);
			return takmicenje.get();
		}
		return null;
	}

	@Override
	public Page<Linija> find(String destinacija, Long prevoznikId, Double maksimalnaCena, Integer pageNo) {
		if(destinacija==null){
			destinacija="";
		}
		
		if(maksimalnaCena==null) {
			maksimalnaCena=Double.MAX_VALUE;
		}
		
		if(prevoznikId==null) {
			return rep.findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThanEqual(destinacija, maksimalnaCena, PageRequest.of(pageNo, 2));
		}else {
			return rep.findByDestinacijaIgnoreCaseContainsAndPrevoznikIdAndCenaKarteLessThanEqual(destinacija, prevoznikId, maksimalnaCena, PageRequest.of(pageNo, 2));
		}
	}
	@Override
	public Integer getZauzetaMesta(Long id) {
		int brojZauzetihMesta=0;
		List<Rezervacija> rezervacije=rezervacijaRepository.findAll();
		for(Rezervacija rezervacija:rezervacije) {
			if(rezervacija.getLinija().getId()==id) {
				brojZauzetihMesta++;
			}
		}
		return brojZauzetihMesta;
		
	}

}
