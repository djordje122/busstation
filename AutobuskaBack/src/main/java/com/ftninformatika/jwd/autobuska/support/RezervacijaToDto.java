package com.ftninformatika.jwd.autobuska.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.autobuska.web.dto.RezervacijaDto;
import com.ftninformatika.jwd.autobuska.model.Rezervacija;

@Component
public class RezervacijaToDto implements Converter<Rezervacija, RezervacijaDto>{

	@Override
	public RezervacijaDto convert(Rezervacija source) {
		RezervacijaDto dto=new RezervacijaDto();
		dto.setId(source.getId());
		dto.setIdLinije(source.getLinija().getId());
		dto.setDestinacijaLinije(source.getLinija().getDestinacija());
		return dto;
	}
	
	public List <RezervacijaDto> convert(List <Rezervacija> list) {
		List <RezervacijaDto> dto=new ArrayList<>();
		for(Rezervacija takmicenje:list) {
			dto.add(convert(takmicenje));
		}
		return dto;
		
	}
	

}
