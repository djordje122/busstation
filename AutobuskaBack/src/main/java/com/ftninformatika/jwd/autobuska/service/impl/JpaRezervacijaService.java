package com.ftninformatika.jwd.autobuska.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.autobuska.model.Rezervacija;
import com.ftninformatika.jwd.autobuska.repository.RezervacijaRepository;
import com.ftninformatika.jwd.autobuska.service.RezervacijaService;
@Service
public class JpaRezervacijaService implements RezervacijaService {
	
	@Autowired 
	private RezervacijaRepository repository;

	@Override
	public Rezervacija findOneById(Long id) {
		// TODO Auto-generated method stub
		return repository.findOneById(id);
	}

	@Override
	public Rezervacija save(Rezervacija rezervacija) {
		// TODO Auto-generated method stub
		return repository.save(rezervacija);
	}

	@Override
	public List<Rezervacija> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public List<Rezervacija> findAllByLinijaId(Long id) {
		// TODO Auto-generated method stub
		return repository.findAllByLinijaId(id);
	}
	
	
	

}
