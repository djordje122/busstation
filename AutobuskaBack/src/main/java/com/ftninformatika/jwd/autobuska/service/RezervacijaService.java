package com.ftninformatika.jwd.autobuska.service;

import java.util.List;

import com.ftninformatika.jwd.autobuska.model.Rezervacija;

public interface RezervacijaService {
	
	Rezervacija findOneById(Long id);
	
	Rezervacija save(Rezervacija rezervacija);
	
	List<Rezervacija> findAll();
	
	List<Rezervacija> findAllByLinijaId(Long id);

}
