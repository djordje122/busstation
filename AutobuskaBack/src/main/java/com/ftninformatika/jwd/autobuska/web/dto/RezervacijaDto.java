package com.ftninformatika.jwd.autobuska.web.dto;

public class RezervacijaDto {
	
	private Long id;
	
	private Long idLinije;
	
	private String destinacijaLinije;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdLinije() {
		return idLinije;
	}

	public void setIdLinije(Long idLinije) {
		this.idLinije = idLinije;
	}

	public String getDestinacijaLinije() {
		return destinacijaLinije;
	}

	public void setDestinacijaLinije(String destinacijaLinije) {
		this.destinacijaLinije = destinacijaLinije;
	}
	
	

}
