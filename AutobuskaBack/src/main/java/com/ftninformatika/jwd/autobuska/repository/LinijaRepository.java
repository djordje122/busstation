package com.ftninformatika.jwd.autobuska.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.autobuska.model.Linija;

@Repository
public interface LinijaRepository extends JpaRepository<Linija, Long> {
	
	
	Linija findOneById(Long id);
	
	Page<Linija> findByDestinacijaIgnoreCaseContainsAndPrevoznikIdAndCenaKarteLessThanEqual
	(String destinacija,Long prevoznikId,Double maksimalnaCena,Pageable pageable);
	
	Page<Linija> findByDestinacijaIgnoreCaseContainsAndCenaKarteLessThanEqual
	(String destinacija,Double maksimalnaCena,Pageable pageable);
	
	
}
