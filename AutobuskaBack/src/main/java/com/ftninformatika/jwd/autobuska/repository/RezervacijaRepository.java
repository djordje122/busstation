package com.ftninformatika.jwd.autobuska.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.autobuska.model.Rezervacija;

@Repository
public interface RezervacijaRepository extends JpaRepository<Rezervacija, Long> {
	
	Rezervacija findOneById(Long id);
	
	List<Rezervacija> findAllByLinijaId(Long id);

}
