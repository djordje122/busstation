package com.ftninformatika.jwd.autobuska.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.autobuska.web.dto.LinijaDto;
import com.ftninformatika.jwd.autobuska.model.Linija;
import com.ftninformatika.jwd.autobuska.service.LinijaService;
import com.ftninformatika.jwd.autobuska.service.PrevoznikService;

@Component
public class DtoToLinija implements Converter<LinijaDto, Linija> {
	
	@Autowired
	private LinijaService service;
	
	@Autowired
	private PrevoznikService prService;

	@Override
	public Linija convert(LinijaDto dto) {
		Linija entity;
		if(dto.getId()==null) {
			entity=new Linija();
		}else {
			entity=service.findOneById(dto.getId());
		}
		
		
		if(entity!=null) {
			entity.setBrojMesta(dto.getBrojMesta());
			entity.setCenaKarte(dto.getCenaKarte());
			entity.setDestinacija(dto.getDestinacija());
			entity.setVremePolaska(getLocalDate(dto.getVremePolaska()));
			
			
			
			if(dto.getPrevoznikId()!=null && prService.findOneById(dto.getPrevoznikId())!=null) {
				entity.setPrevoznik(prService.findOneById(dto.getPrevoznikId()));
			}
			
			

		}
		
		return entity;
	}
	 private LocalDate getLocalDate(String dateTime) throws DateTimeParseException {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        return LocalDate.parse(dateTime, formatter);
	    }

	

}
