package com.ftninformatika.jwd.autobuska.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.autobuska.web.dto.RezervacijaDto;
import com.ftninformatika.jwd.autobuska.model.Rezervacija;
import com.ftninformatika.jwd.autobuska.service.LinijaService;
import com.ftninformatika.jwd.autobuska.service.RezervacijaService;

@Component
public class DtoToRezervacija implements Converter<RezervacijaDto, Rezervacija> {

	@Autowired
	private RezervacijaService service;

	@Autowired
	private LinijaService linService;

	@Override
	public Rezervacija convert(RezervacijaDto dto) {
		Rezervacija entity;
		if (dto.getId() == null) {
			entity = new Rezervacija();
		} else {
			entity = service.findOneById(dto.getId());
		}

		if (dto.getIdLinije() != null && linService.findOneById(dto.getIdLinije()) != null) {
			entity.setLinija(linService.findOneById(dto.getIdLinije()));
		}

		return entity;
	}
}
