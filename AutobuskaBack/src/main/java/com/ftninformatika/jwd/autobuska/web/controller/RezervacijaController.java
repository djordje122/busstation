package com.ftninformatika.jwd.autobuska.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.autobuska.web.dto.RezervacijaDto;
import com.ftninformatika.jwd.autobuska.model.Linija;
import com.ftninformatika.jwd.autobuska.model.Rezervacija;
import com.ftninformatika.jwd.autobuska.service.LinijaService;
import com.ftninformatika.jwd.autobuska.service.RezervacijaService;
import com.ftninformatika.jwd.autobuska.support.DtoToRezervacija;
import com.ftninformatika.jwd.autobuska.support.RezervacijaToDto;

@RestController
@RequestMapping(value = "/api/rezervacije",produces = MediaType.APPLICATION_JSON_VALUE)
public class RezervacijaController {
	
	@Autowired
	private RezervacijaService service;
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private RezervacijaToDto toDto;
	
	@Autowired
	private DtoToRezervacija toEntity;
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<RezervacijaDto>> getAll() {
		List<Rezervacija> list=service.findAll();
		return new ResponseEntity<>(toDto.convert(list),HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RezervacijaDto> create(@Valid @RequestBody RezervacijaDto vinoDto) { 
		Rezervacija vino = toEntity.convert(vinoDto);
		Long linijaId=vino.getLinija().getId();
		Linija lin=linijaService.findOneById(linijaId);
		if(lin.getBrojMesta()-service.findAllByLinijaId(linijaId).size()>0) {
		Rezervacija sacuvaniTakmicar = service.save(vino);
		return new ResponseEntity<>(toDto.convert(sacuvaniTakmicar), HttpStatus.CREATED);
		}else {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
}
