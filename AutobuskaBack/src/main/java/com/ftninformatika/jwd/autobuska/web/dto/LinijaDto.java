package com.ftninformatika.jwd.autobuska.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

public class LinijaDto {
	
	private Long id;

	@PositiveOrZero
	private Integer brojMesta;

	private Double cenaKarte;

	private String vremePolaska;
	
	@NotBlank
	private String destinacija;

	private Long prevoznikId;
	
	private String prevoznikNaziv;
	
	private String prevoznikPIB;
	
	

	public String getPrevoznikPIB() {
		return prevoznikPIB;
	}

	public void setPrevoznikPIB(String prevoznikPIB) {
		this.prevoznikPIB = prevoznikPIB;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(Integer brojMesta) {
		this.brojMesta = brojMesta;
	}

	public Double getCenaKarte() {
		return cenaKarte;
	}

	public void setCenaKarte(Double cenaKarte) {
		this.cenaKarte = cenaKarte;
	}

	

	public String getVremePolaska() {
		return vremePolaska;
	}

	public void setVremePolaska(String vremePolaska) {
		this.vremePolaska = vremePolaska;
	}

	public String getDestinacija() {
		return destinacija;
	}

	public void setDestinacija(String destinacija) {
		this.destinacija = destinacija;
	}

	public Long getPrevoznikId() {
		return prevoznikId;
	}

	public void setPrevoznikId(Long prevoznikId) {
		this.prevoznikId = prevoznikId;
	}

	public String getPrevoznikNaziv() {
		return prevoznikNaziv;
	}

	public void setPrevoznikNaziv(String prevoznikNaziv) {
		this.prevoznikNaziv = prevoznikNaziv;
	}
	
	

}
