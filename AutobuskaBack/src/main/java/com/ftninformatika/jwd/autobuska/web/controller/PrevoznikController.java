package com.ftninformatika.jwd.autobuska.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.autobuska.web.dto.PrevoznikDto;
import com.ftninformatika.jwd.autobuska.model.Prevoznik;
import com.ftninformatika.jwd.autobuska.service.PrevoznikService;
import com.ftninformatika.jwd.autobuska.support.DtoToPrevoznik;
import com.ftninformatika.jwd.autobuska.support.PrevoznikToDto;

@RestController
@RequestMapping(value = "/api/prevoznici",produces = MediaType.APPLICATION_JSON_VALUE)
public class PrevoznikController {
	
	@Autowired
	private PrevoznikService service;
	
	@Autowired
	private PrevoznikToDto toDto;
	
	@Autowired
	private DtoToPrevoznik toEntity;
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<PrevoznikDto>> getAll() {
		List<Prevoznik> list=service.findAll();
		return new ResponseEntity<>(toDto.convert(list),HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<PrevoznikDto> getOne(@PathVariable Long id) {
		Prevoznik prijava = service.findOneById(id); 

		if (prijava != null) {
			return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrevoznikDto> create(@Valid @RequestBody PrevoznikDto vinoDto) { 
		Prevoznik vino = toEntity.convert(vinoDto);

		Prevoznik sacuvaniTakmicar = service.save(vino);

		return new ResponseEntity<>(toDto.convert(sacuvaniTakmicar), HttpStatus.CREATED);
	}
	

}
