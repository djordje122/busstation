package com.ftninformatika.jwd.autobuska.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.autobuska.web.dto.PrevoznikDto;
import com.ftninformatika.jwd.autobuska.model.Prevoznik;

@Component
public class PrevoznikToDto implements Converter<Prevoznik, PrevoznikDto> {

	@Override
	public PrevoznikDto convert(Prevoznik source) {
		PrevoznikDto dto=new PrevoznikDto();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setAdresa(source.getAdresa());
		dto.setPIB(source.getPIB());
		return dto;
		
	}
	
	public List <PrevoznikDto> convert(List <Prevoznik> list) {
		List <PrevoznikDto> dto=new ArrayList<>();
		for(Prevoznik takmicenje:list) {
			dto.add(convert(takmicenje));
		}
		return dto;
		
	}

}
