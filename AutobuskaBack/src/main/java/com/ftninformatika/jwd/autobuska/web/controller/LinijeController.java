package com.ftninformatika.jwd.autobuska.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.autobuska.web.dto.LinijaDto;
import com.ftninformatika.jwd.autobuska.model.Linija;
import com.ftninformatika.jwd.autobuska.service.LinijaService;
import com.ftninformatika.jwd.autobuska.support.DtoToLinija;
import com.ftninformatika.jwd.autobuska.support.LinijaToDto;

@RestController
@RequestMapping(value = "/api/linije",produces = MediaType.APPLICATION_JSON_VALUE)
public class LinijeController {
	
	@Autowired
	private LinijaService service;
	
	@Autowired
	private LinijaToDto toDto;
	
	@Autowired
	private DtoToLinija toEntity;
	
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<LinijaDto>> getAll(
			@RequestParam(required=false) String destinacija, 
			@RequestParam(required=false) Long prevoznikId,
			@RequestParam(required=false) Double maksimalnaCena,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		

		Page<Linija> page = service.find(destinacija, prevoznikId,maksimalnaCena, pageNo) ;

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<LinijaDto> getOne(@PathVariable Long id) {
		Linija prijava = service.findOneById(id); 

		if (prijava != null) {
			return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDto> create(@Valid @RequestBody LinijaDto vinoDto) { 
		Linija vino = toEntity.convert(vinoDto);

		Linija sacuvaniTakmicar = service.save(vino);

		return new ResponseEntity<>(toDto.convert(sacuvaniTakmicar), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDto> update(@PathVariable Long id, @Valid @RequestBody LinijaDto vinoDto) { 

		if (!id.equals(vinoDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// ukoliko ne postoji vino
		if (service.findOneById(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		
		Linija vino = toEntity.convert(vinoDto); 

		Linija sacuvanoVino = service.update(vino);

		return new ResponseEntity<>(toDto.convert(sacuvanoVino), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Linija obrisaniTakmicar = service.findOneById(id);

		if (obrisaniTakmicar != null) {
			service.delete(obrisaniTakmicar.getId());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
