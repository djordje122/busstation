package com.ftninformatika.jwd.autobuska.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.autobuska.web.dto.PrevoznikDto;
import com.ftninformatika.jwd.autobuska.model.Prevoznik;
import com.ftninformatika.jwd.autobuska.service.PrevoznikService;

@Component
public class DtoToPrevoznik implements Converter<PrevoznikDto, Prevoznik> {
	
	@Autowired 
	private PrevoznikService service;

	@Override
	public Prevoznik convert(PrevoznikDto dto) {
		Prevoznik entity;
		if(dto.getId()==null) {
			entity=new Prevoznik();
		}else {
			entity=service.findOneById(dto.getId());
		}
		
		
		if(entity!=null) {
			entity.setNaziv(dto.getNaziv());
			entity.setAdresa(dto.getAdresa());
			entity.setPIB(dto.getPIB());
			

		}
		
		return entity;
	}

}
