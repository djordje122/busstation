package com.ftninformatika.jwd.autobuska.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.autobuska.model.Prevoznik;
import com.ftninformatika.jwd.autobuska.repository.PrevoznikRepository;
import com.ftninformatika.jwd.autobuska.service.PrevoznikService;

@Service
public class JpaPrevoznikService implements PrevoznikService {
	
	@Autowired 
	private PrevoznikRepository rep;

	@Override
	public Prevoznik findOneById(Long id) {
		// TODO Auto-generated method stub
		return rep.findOneById(id);
	}

	@Override
	public List<Prevoznik> findAll() {
		// TODO Auto-generated method stub
		return rep.findAll();
	}

	@Override
	public Prevoznik save(Prevoznik prevoznik) {
		// TODO Auto-generated method stub
		return rep.save(prevoznik);
	}

}
