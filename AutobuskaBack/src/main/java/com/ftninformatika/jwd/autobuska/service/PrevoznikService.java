package com.ftninformatika.jwd.autobuska.service;

import java.util.List;

import com.ftninformatika.jwd.autobuska.model.Prevoznik;

public interface PrevoznikService {
	
	Prevoznik findOneById(Long id);
	
	List <Prevoznik> findAll();
	
	Prevoznik save(Prevoznik prevoznik);
}
