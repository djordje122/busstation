package com.ftninformatika.jwd.autobuska.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.autobuska.web.dto.LinijaDto;
import com.ftninformatika.jwd.autobuska.model.Linija;
import com.ftninformatika.jwd.autobuska.service.LinijaService;

@Component 
public class LinijaToDto implements Converter<Linija, LinijaDto> {
	
	@Autowired
	private LinijaService service;

	@Override
	public LinijaDto convert(Linija source) {
		LinijaDto dto=new LinijaDto();
		dto.setId(source.getId());
		dto.setBrojMesta(source.getBrojMesta()-service.getZauzetaMesta(source.getId()));
		dto.setCenaKarte(source.getCenaKarte());
		dto.setVremePolaska(source.getVremePolaska().toString());
		dto.setDestinacija(source.getDestinacija());
		dto.setPrevoznikId(source.getPrevoznik().getId());
		dto.setPrevoznikNaziv(source.getPrevoznik().getNaziv());
		dto.setPrevoznikNaziv(source.getPrevoznik().getNaziv());
		dto.setPrevoznikPIB(source.getPrevoznik().getPIB());
		return dto;
	}
	
	public List <LinijaDto> convert(List <Linija> list) {
		List <LinijaDto> dto=new ArrayList<>();
		for(Linija takmicenje:list) {
			dto.add(convert(takmicenje));
		}
		return dto;
		
	}
}
