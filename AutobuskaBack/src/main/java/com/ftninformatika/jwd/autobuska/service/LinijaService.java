package com.ftninformatika.jwd.autobuska.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.autobuska.model.Linija;

public interface LinijaService {
	
Linija findOneById(Long id);
	
	List<Linija> findAll(Integer pageNo);
	
	Linija save(Linija t);
	
	Linija update(Linija t);
	
	Linija delete(Long id);
	
	Page <Linija> find(String destinacija,Long prevoznikId,Double maksimalnaCena,Integer pageNo);
	
	Integer getZauzetaMesta(Long id);

}
