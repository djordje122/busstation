INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');

/*lozinke korisnika su iste kao korisnicka imena*/

INSERT INTO prevoznik (naziv, adresa, pib) VALUES ( 'Lasta', 'Puskinova 12',123);
INSERT INTO prevoznik (naziv, adresa, pib) VALUES ( 'Duga', 'Gogoljeva 11',321);
INSERT INTO prevoznik (naziv, adresa, pib) VALUES ( 'FlixBus', 'Tolstojeva 10',213);


INSERT INTO linija (broj_mesta, cena_karte, vreme_polaska,destinacija,prevoznik_id) VALUES ( 15, 100.00 ,'2000-12-01','Nis' ,1);
INSERT INTO linija (broj_mesta, cena_karte, vreme_polaska,destinacija,prevoznik_id) VALUES ( 20, 350.00,'2000-11-01' ,'Beograd ' ,2);
INSERT INTO linija (broj_mesta, cena_karte, vreme_polaska,destinacija,prevoznik_id) VALUES ( 1, 850.50,'2000-10-01','Surdulica',3);


INSERT INTO rezervacija (linija_id) VALUES ( 2);

INSERT INTO rezervacija (linija_id) VALUES ( 2);


