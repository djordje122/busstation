import React from "react";
import { Row, Col, Button, Table, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withNavigation, withParams } from "../../routeconf";

class Linije extends React.Component {
  constructor(props) {
    super(props);

    let search = {
      destinacija: "",
      prevoznikId: "",
      maksimalnaCena: "",
      totalPages: "",
    };

    this.state = {
      linije: [],
      search: search,
      prevoznici: [],
      pageNo: 0,
      totalPages: 2,
      check: true,
    };
  }

  componentDidMount() {
    this.getLinije(0);
    this.getPrevoznici();
  }

  getLinije(newPageNo) {
    if (newPageNo < 0) {
      newPageNo = 0;
    }

    console.log(newPageNo);

    let search = this.state.search;
    let config = {
      params: {
        destinacija: search.destinacija,
        prevoznikId: search.prevoznikId,
        maksimalnaCena: search.maksimalnaCena,
        pageNo: newPageNo,
      },
    };

    OGAxios.get("/linije", config)

      .then((res) => {
        this.setState({ pageNo: newPageNo });
        console.log(res);
        this.setState({
          linije: res.data,
          totalPages: res.headers["total-pages"],
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getPrevoznici() {
    OGAxios.get("/prevoznici")
      .then((res) => {
        console.log(res);
        this.setState({ prevoznici: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onSearchInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let search = this.state.search;
    search[name] = value;
    console.log(search);
    this.setState({ search: search});
  }

  // onClickStats() {
  //   if (this.state.check == true) {
  //     this.setState({ check: false });
  //   } else {
  //     this.setState({ check: true });
  //   }
  // }

  goToAddLinije() {
    this.props.navigate("/linija/add");
  }

  goToEdit(id) {
    this.props.navigate("/linija/edit/" + id);
  }

  createRezervacija(id) {
    let dto = {
      idLinije: id
    };
    console.log("dto: " + JSON.stringify(dto));
    try {
      OGAxios.post("/rezervacije", dto).then((res) => {
        console.log(res);
        alert("Uspesno ste rezervisali kartu")
        window.location.reload(); 
      });
    } catch (err) {
      alert(err)
      console.log(err);
    }
  }



  deleteLinije(id) {
    OGAxios.delete("/linije/" + id)
      .then((res) => {
        console.log(res);
        alert("uspeh")
        window.location.reload();

      })
      .catch((err) => {
        console.log(err);
        window.location.reload(); 
      });
  }

  renderPrevozniciInDropDown() {
    return this.state.prevoznici.map((prevoznik) => {
      return (
        <option value={prevoznik.id} key={prevoznik.id}>
          {prevoznik.naziv}
        </option>
      );
    });
  }

  renderLinije() {
    return this.state.linije.map((lin) => {
      return (
        <tr key={lin.id}>
          <td>{lin.prevoznikNaziv}</td>
          <td>{lin.destinacija}</td>
          <td>{lin.brojMesta}</td>
          <td>{lin.vremePolaska}</td>
          <td>{lin.cenaKarte}</td>

         <td><Button  onClick={() => this.createRezervacija(lin.id)}>
                Rezervisi
              </Button></td>

              {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.deleteLinije(lin.id)}>Obrisi</Button></td>
                        : <td></td>}
          {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
            <td><Button variant="warning" onClick={() => this.goToEdit(lin.id)}>Edit</Button></td>
              : <td></td>}
          <td>
            {" "}
             
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <>
        {window.localStorage["role"] == "ROLE_ADMIN" ||
        window.localStorage["role"] == "ROLE_KORISNIK" ? (
          <div>
            <div className="search">
              <Form style={{ width: "100%" }}>
                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Destinacija</Form.Label>
                      <Form.Control
                        name="destinacija"
                        as="input"
                        type="text"
                        onChange={(e) => this.onSearchInputChange(e)}
                      ></Form.Control>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Prevoznik</Form.Label>
                      <Form.Select
                        name="prevoznikId"
                        as="input"
                        type="number"
                        onChange={(e) => this.onSearchInputChange(e)}
                      >
                        <option value="">Izaberi Prevoznika</option>
                        {this.renderPrevozniciInDropDown()}
                      </Form.Select>
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Maksimalna Cena</Form.Label>
                      <Form.Control
                        name="maksimalnaCena"
                        as="input"
                        type="number"
                        min="0"
                        step="50"
                        onChange={(e) => this.onSearchInputChange(e)}
                      ></Form.Control>
                    </Form.Group>
                  </Col>
                </Row>
              </Form>
              <Row>
                <Col>
                  <Button onClick={() => this.getLinije(0)}>
                    Pretrazi
                  </Button>
                </Col>
              </Row>
            </div>
            <br />
            <div className="displayCompetitorTable">
              {window.localStorage["role"] == "ROLE_ADMIN" ? (
                <Button onClick={() => this.goToAddLinije()}>
                  Dodaj liniju
                </Button>
              ) : null}
             <Button 
            style={{ margin: 3, width: 90}}
            disabled={this.state.pageNo==0} onClick={()=>this.getLinije(this.state.pageNo-1)}>
            Previous
          </Button>
          <Button
            style={{ margin: 3, width: 90}}
            disabled={this.state.pageNo==this.state.totalPages-1} onClick={()=>this.getLinije(this.state.pageNo+1)}>
            Next
          </Button>
              <Table>
                <thead>
                  <tr>
                    <th>Naziv prevoznika</th>
                    <th>Destinacija</th>
                    <th>Broj Mesta</th>
                    <th>Datum Polaska</th>
                    <th>Cena Karte</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>{this.renderLinije()}</tbody>
              </Table>
              <br />
            </div>
          </div>
        ) : null}
      </>
    );
  }
}

export default withNavigation(withParams(Linije));
