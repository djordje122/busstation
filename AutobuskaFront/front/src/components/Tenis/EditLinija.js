import React from "react";
import { Row, Col, Button, Table, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withParams, withNavigation } from "../../routeconf";

class EditLinija extends React.Component {
  constructor(props) {
    super(props);

    let edit = {
      brojMesta: "",
      cenaKarte: "",
      destinacija: "",
      vremePolaska: "",
      prevoznikId: "",
    };

    this.state = {
      edit: edit,
      prevoznici: [],
    };
  }

  componentDidMount() {
    this.getLinija(this.props.params.id);
    this.getPrevoznici();
  }

  edit() {
    var params = this.state.edit;
    OGAxios.put("/linije/" + params.id, params)
      .then((res) => {
        console.log(res);
        this.props.navigate("/linije");
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getLinija(id) {
    OGAxios.get("/linije/" + id)
      .then((res) => {
        console.log(res);
        this.setState({ edit: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onEditInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let edit = this.state.edit;
    edit[name] = value;
    console.log(edit);
    this.setState({ edit: edit });
  }

  onInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let nesto = this.state.nesto;
    nesto[name] = value;
    console.log(nesto);
    this.setState({ nesto: nesto });
  }

  getPrevoznici() {
    OGAxios.get("/prevoznici")
      .then((res) => {
        console.log(res);
        this.setState({ prevoznici: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  renderPrevoznici() {
    return this.state.prevoznici.map((prevoznik) => {
      return (
        <option value={prevoznik.id} key={prevoznik.id}>
          {prevoznik.naziv}
        </option>
      );
    });
  }

  render() {
    return (
      <div>
        <div className="search">
          <Form style={{ width: "100%" }}>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Broj mesta</Form.Label>
                  <Form.Control
                    name="brojMesta"
                    as="input"
                    type="number"
                    value={this.state.edit.brojMesta}
                    onChange={(e) => this.onEditInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Cena karte</Form.Label>
                  <Form.Control
                    name="cenaKarte"
                    as="input"
                    type="number"
                    value={this.state.edit.cenaKarte}
                    onChange={(e) => this.onEditInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Destinacija</Form.Label>
                  <Form.Control
                    name="destinacija"
                    type="text"
                    value={this.state.edit.destinacija}
                    onChange={(e) => this.onEditInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Datum polaska</Form.Label>
                  <Form.Control
                    name="vremePolaska"
                    type="date"
                    value={this.state.edit.vremePolaska}
                    onChange={(e) => this.onEditInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>

            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Prevoznik</Form.Label>
                  <Form.Select
                    onChange={(e) => this.onEditInputChange(e)}
                    name="prevoznikId"
                    value={this.state.edit.prevoznikId}
                    as="select"
                  >
                    {/* <option value={-1}></option> */}
                    {this.state.prevoznici.map((prevoznik) => {
                      return (
                        <option value={prevoznik.id} key={prevoznik.id}>
                          {prevoznik.naziv}
                        </option>
                      );
                    })}
                  </Form.Select>
                </Form.Group>
              </Col>
            </Row>
          </Form>
          <Row>
            <Col>
              <Button onClick={() => this.edit()}>Izmeni</Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default withParams(withNavigation(EditLinija));
