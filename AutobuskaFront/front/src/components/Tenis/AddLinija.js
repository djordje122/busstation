import React from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withNavigation } from "../../routeconf";

class AddLinija extends React.Component {
  constructor(props) {
    super(props);

    let params = {
      brojMesta: "",
      cenaKarte: "",
      destinacija: "",
      vremePolaska: "",
      prevoznikId: "",
    };
    this.state = { params: params, prevoznici: [] };
  }

  componentDidMount() {
    this.getPrevoznici();
  }

  getPrevoznici() {
    OGAxios.get("/prevoznici")
      .then((res) => {
        console.log(res);
        this.setState({ prevoznici: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  onInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let params = this.state.params;
    params[name] = value;
    console.log(params);
    this.setState({ params: params });
  }

  renderPrevozniciInDropDown() {
    return this.state.prevoznici.map((prevoznik) => {
      return (
        <option value={prevoznik.id} key={prevoznik.id}>
          {prevoznik.naziv}
        </option>
      );
    });
  }

  createLinija(e) {
    let params = this.state.params;
    let dto = {
      brojMesta: params.brojMesta,
      cenaKarte: params.cenaKarte,
      destinacija: params.destinacija,
      vremePolaska: params.vremePolaska,
      prevoznikId: params.prevoznikId,
    };
    console.log("dto: " + JSON.stringify(dto));
    try {
      OGAxios.post("/linije", dto).then((res) => {
        console.log(res);
        this.props.navigate("/linije");
      });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    return (
      <>
        <div>
          <Form style={{ width: "100%" }}>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Broj Mesta</Form.Label>
                  <Form.Control
                    name="brojMesta"
                    as="input"
                    type="number"
                    min="0"
                    step="1"
                    placeholder="Unesi broj mesta"
                    onChange={(e) => this.onInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>

            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Cena karte</Form.Label>
                  <Form.Control
                    type="number"
                    name="cenaKarte"
                    placeholder="Unesi cenu karta"
                    min="0"
                    step="100"
                    onChange={(e) => this.onInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Destinacija</Form.Label>
                  <Form.Control
                    type="text"
                    name="destinacija"
                    placeholder="Unesi destinaciju"
                    onChange={(e) => this.onInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Datum polaska</Form.Label>
                  <Form.Control
                    type="date"
                    name="vremePolaska"
                    onChange={(e) => this.onInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Prevoznik</Form.Label>
                  <Form.Select
                    name="prevoznikId"
                    onChange={(e) => this.onInputChange(e)}
                  >
                    {this.renderPrevozniciInDropDown()}
                  </Form.Select>
                </Form.Group>
              </Col>
            </Row>
          </Form>
          <br />
          <Row>
            <Col>
              <Button onClick={(e) => this.createLinija(e)}>
                Dodaj liniju
              </Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default withNavigation(AddLinija);
