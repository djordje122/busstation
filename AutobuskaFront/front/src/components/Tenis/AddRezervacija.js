import React from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withParams, withNavigation } from "../../routeconf";

class AddRezervacija extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      idLinije: ""
    };
  }

  componentDidMount() {
    console.log("PROPS" + this.state.competitorId);
  }

  createApplication() {
    let sportApp = this.state.sportApp;
    let dto = {
      competitorId: sportApp.competitorId,
      sport: sportApp.sport,
    };

    try {
      OGAxios.post("/competitors/application/", dto).then((res) => {
        console.log(res);
        this.props.navigate("/competitors");
      });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    return (
      <div>
        <Form.Group>
          <Form.Label>Disciplina</Form.Label>
          <Form.Control
            name="sport"
            as="input"
            type="text"
            onChange={(e) => this.onInputChange(e)}
          ></Form.Control>
        </Form.Group>
        <Button onClick={() => this.createApplication()}>Add</Button>
      </div>
    );
  }

  onInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let sportApp = this.state.sportApp;
    sportApp[name] = value;
    console.log(sportApp);
    this.setState({ sportApp });
  }
}

export default withNavigation(withParams(AddRezervacija));
