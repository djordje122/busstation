import React from "react";
import ReactDOM from "react-dom";
import {
  Route,
  Link,
  HashRouter as Router,
  Routes,
  Navigate,
} from "react-router-dom";
import { Navbar, Nav, Button, Container } from "react-bootstrap";
import Linije from "./components/Tenis/Linije";
import AddLinija from "./components/Tenis/AddLinija";
import AddRezervacija from "./components/Tenis/AddRezervacija";
import EditLinija from "./components/Tenis/EditLinija";
import { logout } from "./services/auth";
import Login from "./components/Login/Login";

class App extends React.Component {
  render() {
    return (
      <>
        <Router>
          <Navbar expand bg="dark" variant="dark">
            <Navbar.Brand as={Link} to="/">
              JWD
            </Navbar.Brand>
            {window.localStorage["role"] == "ROLE_ADMIN" ||
            window.localStorage["role"] == "ROLE_KORISNIK" ? (
              <Nav>
                <Nav.Link as={Link} to="/linije">
                  Linije
                </Nav.Link>
              </Nav>
            ) : null}

            {window.localStorage["jwt"] ? (
              <Button onClick={() => logout()}>Log out</Button>
            ) : (
              <Nav.Link as={Link} to="/login">
                Log in
              </Nav.Link>
            )}
          </Navbar>
          <Container style={{ paddingTop: "10px" }}>
            <Routes>
              <Route path="/login" element={<Login />} />
              <Route path="/linije" element={<Linije />} />
              <Route path="/linija/add" element={<AddLinija />} />
              <Route path="/linija/edit/:id" element={<EditLinija />} />
              <Route
                path="/linija/rezervacije/:id"
                element={<AddRezervacija />}
              />
              <Route path="/login" element={<Login />} />
            </Routes>
          </Container>
        </Router>
      </>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
